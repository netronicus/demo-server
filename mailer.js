"use strict";
const nodemailer = require("nodemailer");
const handlebars = require('handlebars');
const fs = require('fs');
const path = require('path');

class Mailer {
  constructor(){}
  async sendEmail(email){

    let transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 465,
      secure: true,
      auth: {
        user: "demo.apps.raul@gmail.com",
        pass: "Arur8891"
      }
    });

    const filePath = path.join(__dirname, 'html/3-validacion-cuenta.html');
    const source = fs.readFileSync(filePath, 'utf-8').toString();
    const template = handlebars.compile(source);
    const replacements = {
      logo: "logo",
      facebook: "facebook",
      twitter: "twitter",
      youtube: "youtube",
      linkedin: "linkedin",
      google: "google",
    };
    const htmlToSend = template(replacements);

    let info = await transporter.sendMail({
      from: 'Raúl Armenta <demo.apps.raul@gmail.com>',
      to: email,
      subject: "Demo Mail",
      text: "HELLO WORLD",
      html: htmlToSend,
      attachments: [{
          filename: 'logo.png',
          path: __dirname+'/html/images/logo.png',
          cid: 'logo'
        },
        {
          filename: 'facebook.png',
          path: __dirname+'/html/images/facebook.png',
          cid: 'facebook'
        },
        {
          filename: 'twitter.png',
          path: __dirname+'/html/images/twitter.png',
          cid: 'twitter'
        },
        {
          filename: 'youtube.png',
          path: __dirname+'/html/images/youtube.png',
          cid: 'youtube'
        },
        {
          filename: 'linkedin.png',
          path: __dirname+'/html/images/linkedin.png',
          cid: 'linkedin'
        },
        {
          filename: 'google.png',
          path: __dirname+'/html/images/google.png',
          cid: 'google'
        }
      ]
    });

    return {msgID:info.messageId}
  }
}
module.exports = Mailer;