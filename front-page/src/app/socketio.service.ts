import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class SocketioService {
  socket;
  private _sendingEmail:boolean = false;
  get sendingEmail():boolean{
    return this._sendingEmail;
  }
  set sendingEmail(sendingEmail:boolean){
    this._sendingEmail = sendingEmail;
  }
  constructor() { }
  setupSocketConnection() {
    this.socket = io(environment.SOCKET_ENDPOINT);
    this.socket.on('emailSent',()=>{
      this.sendingEmail = false;
      Swal.fire({
        title: '¡Éxito!',
        text: 'El correo ha sido enviado exitosamente',
        icon: 'success',
        confirmButtonText: 'OK'
      })
    });
  }
  sendEmail(email){
    this.sendingEmail = true;
    this.socket.emit('sendEmail', email);
  }
}
