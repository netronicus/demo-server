import { Component } from '@angular/core';
import { SocketioService } from './socketio.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Mailer';
  emailForm = new FormGroup({
    email: new FormControl('',Validators.compose([
      Validators.email,
      Validators.required,
      Validators.pattern(/^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@(yahoo\.com|gmail\.com|msn\.com|hotmail\.com|live\.com|outlook.com)$/)
    ]))
  });

  constructor(
    public socketService: SocketioService,
  ) {}

  ngOnInit() {
    this.socketService.setupSocketConnection();
  }
  sendEmail(){
    this.socketService.sendEmail(this.emailForm.value.email);
  }
  clearEmail(){
    this.emailForm.setValue({
      email: ''
    });
  }
}
