const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const Mailer = require('./mailer');

mailer = new Mailer();

app.get('/', (req, res) => {
  res.send('<h1>Hey Socket.io</h1>');
});

io.on('connection', (socket) => {
  console.log('a user connected');
  socket.on('disconnect', () => {
    console.log('user disconnected');
  });
  socket.on('sendEmail', (email) => {
    mailer.sendEmail(email).then((response)=>{
      console.log(response,socket.id);
      io.sockets.connected[socket.id].emit('emailSent')
    }).catch((err)=>{
      console.log(err);
    });
  });
});

http.listen(3000, () => {
  console.log('listening on *:3000');
});